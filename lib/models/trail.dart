import 'dart:collection';

import 'package:json_annotation/json_annotation.dart';

part 'trail.g.dart';

@JsonSerializable(nullable: false)
class Trail extends Object {

  int id;
  String name;
  String type;
  String summary;
  String difficulty;
  double stars;
  int starVotes;
  String location;
  String url;
  String imgSqSmall;
  String imgSmall;
  String imgSmallMed;
  String imgMedium;
  double length;
  int ascent;
  int descent;
  int high;
  int low;
  double longitude;
  double latitude;
  String conditionStatus;
  String conditionDetails;
  String conditionDate;

  Trail(this.id, this.name, this.type, this.summary, this.difficulty, this.stars,
      this.starVotes, this.location, this.url, this.imgSqSmall, this.imgSmall, this.imgSmallMed, this.imgMedium,
      this.length, this.ascent, this.descent, this.high, this.low, this.longitude, this.latitude, this.conditionStatus,
      this.conditionDetails, this.conditionDate);

  factory Trail.fromJson(Map<String, dynamic> json) => _$TrailFromJson(json);

}
