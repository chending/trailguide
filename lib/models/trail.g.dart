// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Trail _$TrailFromJson(Map<String, dynamic> json) {
  return Trail(
    json['id'] as int,
    json['name'] as String,
    json['type'] as String,
    json['summary'] as String,
    json['difficulty'] as String,
    (json['stars'] as num).toDouble(),
    json['starVotes'] as int,
    json['location'] as String,
    json['url'] as String,
    json['imgSqSmall'] as String,
    json['imgSmall'] as String,
    json['imgSmallMed'] as String,
    json['imgMedium'] as String,
    (json['length'] as num).toDouble(),
    json['ascent'] as int,
    json['descent'] as int,
    json['high'] as int,
    json['low'] as int,
    (json['longitude'] as num).toDouble(),
    (json['latitude'] as num).toDouble(),
    json['conditionStatus'] as String,
    json['conditionDetails'] as String,
    json['conditionDate'] as String,
  );
}

Map<String, dynamic> _$TrailToJson(Trail instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'summary': instance.summary,
      'difficulty': instance.difficulty,
      'stars': instance.stars,
      'starVotes': instance.starVotes,
      'location': instance.location,
      'url': instance.url,
      'imgSqSmall': instance.imgSqSmall,
      'imgSmall': instance.imgSmall,
      'imgSmallMed': instance.imgSmallMed,
      'imgMedium': instance.imgMedium,
      'length': instance.length,
      'ascent': instance.ascent,
      'descent': instance.descent,
      'high': instance.high,
      'low': instance.low,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'conditionStatus': instance.conditionStatus,
      'conditionDetails': instance.conditionDetails,
      'conditionDate': instance.conditionDate,
    };
