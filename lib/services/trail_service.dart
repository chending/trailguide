import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client;
import 'package:trailguide/models/trail.dart';


class TrailService {
  final Client httpClient = Client();
  static const String KEY = "200785484-fbbd1f9954a8856c2b651aee49bbba08";
  static const String API = "https://www.hikingproject.com/data/get-trails?lat=40.0274&lon=-105.2519&maxDistance=10&key=";

  Future<List<Trail>> fetchTrails() async {
    final response = await httpClient
        .get(API + KEY, headers: {"Accept": "application/xml"});
    return compute(parseTrails, response.body);
  }

  static List<Trail> parseTrails(String responseBody) {
    final parsed = json.decode(responseBody);
    final data = parsed['trails'];
    return List<Trail>.from(data.map((i) => Trail.fromJson(i)));
  }

}
