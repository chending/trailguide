

import 'package:flutter/widgets.dart';
import 'package:trailguide/models/trail.dart';
import 'package:trailguide/services/trail_service.dart';

class HomeBloc {

  final TrailService _service;
  HomeBloc(this._service);

  Future<List<Trail>> fetchTrails() async {
    final result = await _service.fetchTrails();
    return result;
  }
}

class HomeBlocProvider extends InheritedWidget {
  final HomeBloc bloc;

  HomeBlocProvider(
      {Key key, Widget child, @required HomeBloc bloc})
      : bloc = bloc,
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static HomeBloc of(BuildContext context) => (context.dependOnInheritedWidgetOfExactType<HomeBlocProvider>()).bloc;

}