import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:trailguide/models/trail.dart';
import 'package:trailguide/ui/detail_page.dart';

import '../blocs/home_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = HomeBlocProvider.of(context);
    return Scaffold(
      body: FutureBuilder<List<Trail>>(
          future: bloc.fetchTrails(),
          builder: (BuildContext context, AsyncSnapshot<List<Trail>> snapshot) {
            if (!snapshot.hasData) {
              return Container();
            }

            final size = snapshot.data.length+1;
            return ListView.builder(
                itemCount: size,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return 
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text("Trail Guide", textAlign: TextAlign.center, style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),)
                      );
                  }

                  final trail = snapshot.data[index - 1];
                  final imageUrl = trail.imgMedium;
                  return Container(
                      child: GestureDetector(
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailScreen(),
                          ),
                    ),
                    child: Card(
                        elevation: 5,
                        child: Padding(
                            padding: EdgeInsets.all(7),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image(
                                      image:
                                          CachedNetworkImageProvider(imageUrl)),
                                  Container(
                                    padding: EdgeInsets.all(4),
                                    child: Text(trail.name,
                                        style: TextStyle(
                                          fontSize: 20,
                                        )),
                                  ),
                                  Container(
                                      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 4),
                                      child: Row(children: [
                                        Text(trail.length.toString() + "KM"),
                                        Container(width: 8,),
                                        Text(trail.ascent.toString() +
                                            "m ascent"),
                                      ])),
                                  Row(
                                    children: [
                                      SmoothStarRating(
                                          starCount: 5,
                                          rating: trail.stars,
                                          size: 20.0,
                                          isReadOnly: true,
                                          filledIconData: Icons.star,
                                          halfFilledIconData: Icons.star_half,
                                          color: Colors.red,
                                          spacing: 0.0),
                                      Text(" ("+trail.starVotes.toString()+")", style: TextStyle(fontSize: 14, color: Colors.grey)),
                                      Container(width: 8,),
                                      Text(trail.location, style: TextStyle(fontSize: 14, color: Colors.grey),)
                                    ],
                                  )

                                ]))),
                  ));
                });
          }),
    );
  }
}
