import 'package:flutter/material.dart';
import 'package:trailguide/services/trail_service.dart';

import 'ui/home_page.dart';
import 'blocs/home_bloc.dart';
import 'models/trail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final trailService = TrailService();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: _homePage()
    );
  }

  InheritedWidget _homePage() {
    final bloc = HomeBloc(trailService);
    return HomeBlocProvider(
      bloc: bloc,
      child: HomePage(),
    );
  }


}
